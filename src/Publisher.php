<?php

namespace IC\Activity;

use IC\Activity\Subscriber\Subscriber;
use IC\Activity\Exceptions\SubscriberException;

class Publisher
{
    /**
     * @var \ArrayObject
     */
    private $subscriberCollection;

    /**
     * @var \ArrayObject
     */
    private $exceptionCollection;

    public function __construct(\ArrayObject $subscriberCollection, \ArrayObject $exceptionCollection)
    {
        $this->setSubscribers($subscriberCollection);
        $this->setExceptions($exceptionCollection);
    }

    public function getSubscribers()
    {
        return $this->subscriberCollection;
    }

    public function setSubscribers(\ArrayObject $subscriberCollection)
    {
        $this->subscriberCollection = $subscriberCollection;
        return $this;
    }

    public function getExceptions()
    {
        return $this->exceptionCollection;
    }

    public function setExceptions(\ArrayObject $exceptionCollection)
    {
        $this->exceptionCollection = $exceptionCollection;
        return $this;
    }

    public function resetExceptions()
    {
        $this->getExceptions()->exchangeArray(array());
        return $this;
    }

    public function hasExceptions()
    {
        return (bool) count($this->exceptionCollection);
    }

    public function notify($message)
    {
        $this->resetExceptions();

        foreach($this->getSubscribers() as $subscriber) {
            try {
                $subscriber->notify($message);
            } catch(SubscriberException $e) {
                $this->exceptionCollection[] = $e;
            }
        }
    }
}
