<?php

namespace IC\Activity\Subscriber;

use IC\Activity\Subscriber\Subscriber as SubscriberInterface;
use IC\Activity\Exceptions\InvalidSubscriberException;

class SubscriberCollection extends \ArrayObject
{
    public function offsetSet($index, $value)
    {
        if($value instanceof SubscriberInterface) {
            parent::offsetSet($index, $value);
        } else {
            throw new InvalidSubscriberException('Subscriber must implement IC\Activity\Subscriber\Subscriber interface');
        }
    }
}
