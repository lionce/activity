<?php

namespace IC\Activity\Subscriber;

abstract class AbstractSubscriber implements Subscriber
{
    abstract public function notify($message);
}
