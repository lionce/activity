<?php

namespace IC\Activity\Subscriber;

interface Subscriber
{
    public function notify($message);
}
