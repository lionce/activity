<?php

namespace IC\Activity\Formatter;

/**
 * Interface ActivityFormatter
 * @package IC\Activity\Formatter
 */
interface ActivityFormatter
{
    /**
     * @param array $activity
     * @return string
     */
    public function format(array $activity = array());
}
