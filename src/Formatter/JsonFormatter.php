<?php

namespace IC\Activity\Formatter;

/**
 * Class JsonFormatter
 * @package IC\Activity\Formatter
 */
class JsonFormatter implements ActivityFormatter
{
    /**
     * @param array $activity
     * @return string
     */
    public function format(array $activity = array())
    {
        return json_encode($activity);
    }
}
