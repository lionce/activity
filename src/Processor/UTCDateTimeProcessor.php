<?php

namespace IC\Activity\Processor;

use Carbon\Carbon;
/**
 * Class UTCDateTimeProcessor
 * @package IC\Activity\Processor
 */
class UTCDateTimeProcessor implements ActivityProcessor
{
    /**
     * @var Carbon
     */
    private $carbon;

    /**
     * UTCDateTimeProcessor constructor.
     * @param Carbon $carbon
     */
    public function __construct(Carbon $carbon)
    {
        $this->carbon = $carbon;
    }

    /**
     * @param array $activity
     * @return array
     */
    public function __invoke(array $activity = array())
    {
        $activity['processors']['UTCDateTimeProcessor'] = Carbon::now('UTC')->toDateTimeString();
        $activity['processors']['UTCTimestampSeconds'] = Carbon::now('UTC')->timestamp;
        return $activity;
    }
}
