<?php

namespace IC\Activity;

use IC\Activity\Handler\ActivityHandler;

class Activity
{
    /**
     * @var array
     */
    private $handlers = array();

    /**
     * @var array
     */
    private $processors = array();

    /**
     * @param ActivityHandler $handler
     * @return $this
     */
    public function pushHandler(ActivityHandler $handler)
    {
        array_push($this->handlers, $handler);
        return $this;
    }

    /**
     * @return mixed
     */
    public function popHandler()
    {
        return array_pop($this->handlers);
    }

    /**
     * @return array
     */
    public function getHandlers()
    {
        return $this->handlers;
    }

    /**
     * @param callable $processor
     * @return $this
     */
    public function pushProcessor(callable $processor)
    {
        array_push($this->processors, $processor);
        return $this;
    }

    /**
     * @return mixed
     */
    public function popProcessor()
    {
        return array_pop($this->processors);
    }

    /**
     * @return array
     */
    public function getProcessors()
    {
        return $this->processors;
    }

    /**
     * @param $event
     * @param array $tags
     */
    public function record($event, array $tags = array())
    {
        $activity = [];

        if(!is_array($event)) {
            $event = array((string) $event);
        }

        $activity['activity'] = $event;
        $activity['tags'] = $tags;

        foreach($this->getProcessors() as $processor) {
            $activity = $processor($activity);
        }

        foreach($this->getHandlers() as $handler) {
            $handler->handle($activity);
        }
    }
}
