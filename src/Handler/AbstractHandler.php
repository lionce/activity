<?php

namespace IC\Activity\Handler;
use IC\Activity\Formatter\ActivityFormatter;

/**
 * Class AbstractHandler
 * @package IC\Activity\Handler
 */
abstract class AbstractHandler implements ActivityHandler
{
    /**
     * @var ActivityFormatter
     */
    private $formatter;

    /**
     * @var array
     */
    private $processors = array();

    /**
     * @param array $activity
     * @return boolean
     */
    public function handle(array $activity = array())
    {
        foreach($this->getProcessors() as $processor) {
            $activity = $processor($activity);
        }

        $activity = $this->getFormatter()->format($activity);

        return $activity;
    }

    /**
     * @param ActivityFormatter $formatter
     * @return $this
     */
    public function setFormatter(ActivityFormatter $formatter)
    {
        $this->formatter = $formatter;
        return $this;
    }

    /**
     * @return ActivityFormatter
     */
    public function getFormatter()
    {
        return $this->formatter;
    }

    /**
     * LIFO functionality
     * @param callable $processor
     * @return $this
     */
    public function pushProcessor(callable $processor)
    {
        array_push($this->processors, $processor);
        return $this;
    }

    /**
     * LIFO functionality
     * @return mixed
     */
    public function popProcessor()
    {
        return array_pop($this->processors);
    }

    /**
     * @return array
     */
    public function getProcessors()
    {
        return $this->processors;
    }
}
