<?php

namespace IC\Activity\Handler;
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Class AmqpHandler
 * @package IC\Activity\Handler
 */
class AmqpHandler extends AbstractHandler
{
    private $connection;
    private $delivery;
    private $queue;

    public function __construct(AMQPConnection $connection, $queue, $delivery = 1)
    {
        $this->connection = $connection;
        $this->queue = $queue;
        $this->delivery = $delivery;
    }

    /**
     * @param array $activity
     * @return boolean
     */
    public function handle(array $activity = array())
    {
        $activity = parent::handle($activity);

        $channel = $this->connection->channel();

        $message = new AMQPMessage($activity, array('delivery_mode' => $this->delivery));
        $channel->basic_publish($message, '', $this->queue);

        $channel->close();
        $this->connection->close();
    }
}
