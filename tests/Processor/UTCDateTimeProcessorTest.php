<?php

use IC\Activity\Processor\UTCDateTimeProcessor;
use Carbon\Carbon;

class UTCDateTimeProcessorTest extends PHPUnit_Framework_TestCase
{

    public function setUp()
    {
        $knownDate = Carbon::create(2001, 5, 21, 12);          // create testing date
        Carbon::setTestNow($knownDate);
    }

    /**
     * @test
     */
    public function should_add_new_array_member_when_given_an_array()
    {
        $data = [
            'message' => 'lorem ipsum',
            'tags' => ['tag1', 'tag2', 'tag3']
        ];

        $result = [
            'message' => 'lorem ipsum',
            'tags' => ['tag1', 'tag2', 'tag3'],
            'processors' => [
                'UTCDateTimeProcessor' => '2001-05-21 12:00:00'
            ]
        ];

        $processor = new UTCDateTimeProcessor(new Carbon());

        $data = $processor($data);

        $this->assertInternalType('array', $data);
        $this->assertSame($result, $data);
    }
}
