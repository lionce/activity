<?php

use IC\Activity\Subscriber\SubscriberCollection;
use IC\Activity\Exceptions\InvalidSubscriberException;
use Tests\Mocks\Subscriber\MockSubscriber;

class SubscriberCollectionTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @expectedException IC\Activity\Exceptions\InvalidSubscriberException
     */
    public function objects_not_implementing_subscriber_interface_throw_exception()
    {
        $collection = new SubscriberCollection();
        $object = new stdClass();
        $collection[] = $object;
    }

    /**
     * @test
     * @expectedException IC\Activity\Exceptions\InvalidSubscriberException
     */
    public function appending_objects_not_implementing_subscriber_interface_throw_exception()
    {
        $collection = new SubscriberCollection();
        $object = new stdClass();
        $collection->append($object);
    }

    /**
     * @test
     * @expectedException IC\Activity\Exceptions\InvalidSubscriberException
     */
    public function appending_multiple_objects_not_implementing_subscriber_interface_throw_exception()
    {
        $collection = new SubscriberCollection();
        $subscriber = new MockSubscriber();
        $object = new stdClass();
        $collection->append(array($subscriber, $object));
    }

    /**
     * @test
     */
    public function adding_subscriber_object_gets_added_to_collection()
    {
        $collection = new SubscriberCollection();
        $subscriber = new MockSubscriber();
        $collection[] = $subscriber;

        $this->assertCount(1, $collection);
    }

    /**
     * @test
     */
    public function appending_subscriber_object_to_collection()
    {
        $collection = new SubscriberCollection();
        $subscriber = new MockSubscriber();
        $collection->append($subscriber);

        $this->assertCount(1, $collection);
    }

    /**
     * @test
     * @expectedException IC\Activity\Exceptions\InvalidSubscriberException
     */
    public function appending_array_of_subscribers_to_collection()
    {
        $collection = new SubscriberCollection();
        $subscriber1 = new MockSubscriber();
        $subscriber2 = new MockSubscriber();
        $collection->append(array($subscriber1, $subscriber2));
    }

    /**
     * @test
     */
    public function appending_a_single_subscriber_to_collection()
    {
        $collection = new SubscriberCollection();
        $subscriber1 = new MockSubscriber();
        $subscriber2 = new MockSubscriber();
        $collection->append($subscriber1);
        $collection->append($subscriber2);

        $this->assertCount(2, $collection);
    }
}
