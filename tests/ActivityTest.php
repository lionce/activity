<?php

use IC\Activity\Activity;

class ActivityTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Activity
     */
    private $activity;

    public function setUp()
    {
        $this->activity = new Activity();
    }

    /**
     * @test
     */
    public function should_not_have_handlers_by_default()
    {
        $this->assertEmpty($this->activity->getHandlers());
    }

    /**
     * @test
     */
    public function should_not_have_processors_by_default()
    {
        $this->assertEmpty($this->activity->getProcessors());
    }

    /**
     * @test
     */
    public function should_get_the_last_handler_set()
    {
        $handler = $this->getMockForAbstractClass('IC\Activity\Handler\AbstractHandler');

        $this->activity->pushHandler($handler);
        $this->assertSame($handler, $this->activity->popHandler());
    }

    /**
     * @test
     */
    public function should_get_the_last_processor_set()
    {
        $processor = new \Tests\Mocks\Processor\MockProcessor();

        $this->activity->pushProcessor($processor);
        $this->assertSame($processor, $this->activity->popProcessor());
    }

    /**
     * @test
     */
    public function recorded_activity_should_be_passed_through_entire_stack()
    {
        $formatter = new \Tests\Mocks\Formatter\MockFormatter();

        $processor = new \Tests\Mocks\Processor\MockProcessor();
        $loremProcessor = new \Tests\Mocks\Processor\MockLoremProcessor();

        $handler = new \Tests\Mocks\Handler\MockHandler();
        $handler->setFormatter($formatter);

        $handler->pushProcessor($processor);

        $this->activity->pushHandler($handler);
        $this->activity->pushProcessor($loremProcessor);

        $this->activity->record('Amazing Event!', ['tag1','tag2']);

        //Ensure processors on activity are processed before dispatching to handlers
        $return = $loremProcessor->get();
        $this->assertArrayHasKey('MockLoremProcessor', $return['processors']);

        //Ensure processors on handlers are executed before formatting
        $return = $processor->get();
        $this->assertArrayHasKey('MockProcessor', $return['processors']);

        //Ensure formatting is the last thing that happens
        $data = $formatter->get();

$example = <<<EOD
Array
(
    [activity] => Array
        (
            [0] => Amazing Event!
        )

    [tags] => Array
        (
            [0] => tag1
            [1] => tag2
        )

    [processors] => Array
        (
            [MockLoremProcessor] => Lorem Ipsum
            [MockProcessor] => MOCKED THIS!
        )

)

EOD;
        $this->assertSame($example, $data);


    }

    /**
     * @test
     */
    public function recorded_activity_is_an_array()
    {
        $formatter = new \Tests\Mocks\Formatter\MockFormatter();

        $processor = new \Tests\Mocks\Processor\MockProcessor();
        $loremProcessor = new \Tests\Mocks\Processor\MockLoremProcessor();

        $handler = new \Tests\Mocks\Handler\MockHandler();
        $handler->setFormatter($formatter);

        $handler->pushProcessor($processor);

        $this->activity->pushHandler($handler);
        $this->activity->pushProcessor($loremProcessor);

        $this->activity->record(['Amazing Event!','Also Important!'], ['tag1','tag2']);

        //Ensure processors on activity are processed before dispatching to handlers
        $return = $loremProcessor->get();
        $this->assertArrayHasKey('MockLoremProcessor', $return['processors']);

        //Ensure processors on handlers are executed before formatting
        $return = $processor->get();
        $this->assertArrayHasKey('MockProcessor', $return['processors']);

        //Ensure formatting is the last thing that happens
        $data = $formatter->get();

        $example = <<<EOD
Array
(
    [activity] => Array
        (
            [0] => Amazing Event!
            [1] => Also Important!
        )

    [tags] => Array
        (
            [0] => tag1
            [1] => tag2
        )

    [processors] => Array
        (
            [MockLoremProcessor] => Lorem Ipsum
            [MockProcessor] => MOCKED THIS!
        )

)

EOD;
        $this->assertSame($example, $data);


    }
}
