<?php
namespace  Tests\Mocks\Processor;

use IC\Activity\Processor\ActivityProcessor;

class MockLoremProcessor implements ActivityProcessor
{
    private $value;

    public function get()
    {
        return $this->value;
    }

    public function __invoke(array $activity = array())
    {
        $activity['processors']['MockLoremProcessor'] = 'Lorem Ipsum';

        $this->value = $activity;
        return $activity;
    }
}
