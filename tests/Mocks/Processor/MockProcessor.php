<?php
namespace  Tests\Mocks\Processor;

use IC\Activity\Processor\ActivityProcessor;

class MockProcessor implements ActivityProcessor
{
    private $value;

    public function get()
    {
        return $this->value;
    }

    public function __invoke(array $activity = array())
    {
        $activity['processors']['MockProcessor'] = 'MOCKED THIS!';

        $this->value = $activity;
        return $activity;
    }
}
