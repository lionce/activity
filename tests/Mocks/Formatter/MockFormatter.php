<?php

namespace Tests\Mocks\Formatter;

use IC\Activity\Formatter\ActivityFormatter;

class MockFormatter implements ActivityFormatter
{

    private $value;

    public function get()
    {
        return $this->value;
    }

    public function format(array $activity = array())
    {
        $this->value = print_r($activity, true);
        return $this->value;
    }

}
