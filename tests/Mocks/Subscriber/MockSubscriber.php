<?php

namespace Tests\Mocks\Subscriber;

use IC\Activity\Subscriber\AbstractSubscriber;

class MockSubscriber extends AbstractSubscriber
{
    public $notified = false;

    public function notify($message)
    {
        $this->notified = true;
    }
}
