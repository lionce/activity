<?php

namespace Tests\Mocks\Subscriber;

use IC\Activity\Exceptions\SubscriberException;
use IC\Activity\Subscriber\AbstractSubscriber;

class MockExceptionSubscriber extends AbstractSubscriber
{
    public $notified = false;

    public function notify($message)
    {
        $this->notified = true;

        throw new SubscriberException('Mock Exception Subscriber');
    }
}
