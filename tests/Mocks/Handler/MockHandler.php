<?php

namespace Tests\Mocks\Handler;

use IC\Activity\Handler\AbstractHandler;

class MockHandler extends AbstractHandler
{
    private $value;

    public function get()
    {
        return $this->value;
    }

    public function handle(array $activity = array())
    {
        $activity = parent::handle($activity);

        $this->value = $activity;
        return $activity;
    }
}
