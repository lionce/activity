<?php

use Mockery as m;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;
use IC\Activity\Handler\AmqpHandler;
use Tests\Mocks\Processor\MockProcessor;
use IC\Activity\Formatter\JsonFormatter;

class AmqpHandlerTest extends PHPUnit_Framework_TestCase
{
    private $mockedConnection;
    private $mockedChannel;
    private $queue;

    public function tearDown()
    {
        m::close();
    }

    public function setUp()
    {
        $this->mockedConnection = m::mock('PhpAmqpLib\Connection\AMQPConnection');
        $this->mockedChannel = m::mock('PhpAmqpLib\Channel\AMQPChannel');
        $this->queue = 'default';
    }

    /**
     * @test
     */
    public function handler_should_send_formatted_message_to_amqp_server()
    {
        $this->mockedConnection->shouldReceive('channel')->once()->andReturn($this->mockedChannel);
        $this->mockedChannel->shouldReceive('basic_publish')->with(m::type('PhpAmqpLib\Message\AMQPMessage'), '', $this->queue)->once()->andReturn(true);
        $this->mockedChannel->shouldReceive('close')->once()->andReturn(true);
        $this->mockedConnection->shouldReceive('close')->once()->andReturn(true);


        $handler = new AmqpHandler($this->mockedConnection, $this->queue);
        $handler->pushProcessor(new MockProcessor());
        $handler->setFormatter(new JsonFormatter());

        $activity = [
            'activity' => [
                'Something Happened!',
                'Another thing happened.'
            ],
            'tags' => ['tag1', 'tag2'],
        ];

        $handler->handle($activity);
    }



}
