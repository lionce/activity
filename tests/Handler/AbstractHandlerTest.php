<?php

use Tests\Mocks\Processor\MockProcessor;
use Tests\Mocks\Formatter\MockFormatter;
use Tests\Mocks\Handler\MockHandler;

class AbstractHandlerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var AbstractHandler
     */
    private $handler;

    public function setUp()
    {
        $this->handler = $this->getMockForAbstractClass('IC\Activity\Handler\AbstractHandler');
    }

    /**
     * @test
     */
    public function should_not_have_a_formatter_by_default()
    {
        $this->assertNull($this->handler->getFormatter());
    }

    /**
     * @test
     */
    public function should_retrieve_the_same_formatter_that_is_set()
    {
        $formatter = $this->getMock('IC\Activity\Formatter\ActivityFormatter');

        $this->handler->setFormatter($formatter);

        $this->assertSame($formatter, $this->handler->getFormatter());
    }

    /**
     * @test
     */
    public function should_not_have_processors_by_default()
    {
        $this->assertEmpty($this->handler->popProcessor());
    }

    /**
     * @test
     */
    public function should_retrieve_the_last_processor_added_to_the_stack()
    {
        $data = [];

        $processor1 = new MockProcessor();
        $processor2 = new MockProcessor();
        $processor3 = new MockProcessor();

        $this->handler
            ->pushProcessor($processor1)
            ->pushProcessor($processor2)
            ->pushProcessor($processor3)
        ;

        $this->assertSame($processor3, $this->handler->popProcessor());
        $this->assertNotSame($processor3, $this->handler->popProcessor());
    }

    /**
     * @test
     */
    public function should_return_all_registered_processors()
    {
        $processor1 = new MockProcessor();
        $processor2 = new MockProcessor();
        $processor3 = new MockProcessor();

        $this->handler
            ->pushProcessor($processor1)
            ->pushProcessor($processor2)
            ->pushProcessor($processor3)
        ;

        $this->assertCount(3, $this->handler->getProcessors());
    }

    /**
     * @test
     */
    public function handle_should_run_all_processors_and_return_formatted_result()
    {
        $processor = new MockProcessor();
        $formatter = new MockFormatter();

        $handler = new MockHandler();
        $handler->pushProcessor($processor);
        $handler->setFormatter($formatter);

        $activity = [
            'activity' => ['Something Happened!'],
            'tags' => []
        ];

        $handler->handle($activity);

        //The following ->get() methods ONLY exist on the mock objects for testing introspection.

        $this->assertArrayHasKey('processors', $processor->get());

$example = <<<'EOD'
Array
(
    [activity] => Array
        (
            [0] => Something Happened!
        )

    [tags] => Array
        (
        )

    [processors] => Array
        (
            [MockProcessor] => MOCKED THIS!
        )

)

EOD;

        $this->assertSame($example, $formatter->get());
        $this->assertSame($example, $handler->get());

    }

}
