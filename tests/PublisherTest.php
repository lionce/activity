<?php

namespace Tests;

use IC\Activity\Publisher;
use IC\Activity\Subscriber\ExceptionCollection;
use IC\Activity\Subscriber\SubscriberCollection;
use IC\Activity\Exceptions\SubscriberException;
use Mockery\Mock;
use Tests\Mocks\Subscriber\MockExceptionSubscriber;
use Tests\Mocks\Subscriber\MockSubscriber;

class PublisherTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function assert_collections_set_are_returned()
    {
        $subscribers = new SubscriberCollection();
        $exceptions = new ExceptionCollection();

        $publisher = new Publisher($subscribers, $exceptions);

        $this->assertSame($subscribers, $publisher->getSubscribers());
        $this->assertSame($exceptions, $publisher->getExceptions());
    }

    /**
     * @test
     */
    public function assert_no_exceptions_by_default()
    {
        $subscribers = new SubscriberCollection();
        $exceptions = new ExceptionCollection();

        $publisher = new Publisher($subscribers, $exceptions);

        $this->assertFalse($publisher->hasExceptions());
    }

    /**
     * @test
     */
    public function assert_reset_of_exception_collection()
    {
        $subscribers = new SubscriberCollection();
        $exceptions = new ExceptionCollection();

        $publisher = new Publisher($subscribers, $exceptions);

        $this->assertFalse($publisher->hasExceptions());

        $exception = new SubscriberException();

        $publisher->getExceptions()->append($exception);

        $this->assertTrue($publisher->hasExceptions());

        $publisher->resetExceptions();

        $this->assertFalse($publisher->hasExceptions());
    }

    /**
     * @test
     */
    public function assert_all_subscribers_are_notified()
    {
        $subscribers = new SubscriberCollection();

        $subscribers->append(new MockSubscriber());
        $subscribers->append(new MockSubscriber());
        $subscribers->append(new MockSubscriber());

        $this->assertCount(3, $subscribers);

        $exceptions = new ExceptionCollection();
        $publisher = new Publisher($subscribers, $exceptions);

        $this->assertFalse($publisher->hasExceptions());

        $publisher->notify(['activity' => ['msg1', 'msg2', 'msg3'], 'tags' => ['tag1', 'tag2']]);

        //Make sure each subscriber was notified
        foreach($publisher->getSubscribers() as $sub) {
            $this->assertTrue($sub->notified);
        }

        $this->assertFalse($publisher->hasExceptions());
    }

    /**
     * @test
     */
    public function assert_all_subscriber_exceptions_are_caught()
    {
        $subscribers = new SubscriberCollection();

        $subscribers->append(new MockExceptionSubscriber());
        $subscribers->append(new MockSubscriber());
        $subscribers->append(new MockSubscriber());

        $this->assertCount(3, $subscribers);

        $exceptions = new ExceptionCollection();
        $publisher = new Publisher($subscribers, $exceptions);

        $this->assertFalse($publisher->hasExceptions());

        $publisher->notify(['activity' => ['msg1', 'msg2', 'msg3'], 'tags' => ['tag1', 'tag2']]);

        //Make sure each subscriber was notified
        foreach($publisher->getSubscribers() as $sub) {
            $this->assertTrue($sub->notified);
        }

        $this->assertTrue($publisher->hasExceptions());
        $this->assertCount(1, $publisher->getExceptions());

        $resultsExceptions = $publisher->getExceptions();
        $this->assertInstanceOf('IC\Activity\Exceptions\SubscriberException', $resultsExceptions[0]);
    }
}
