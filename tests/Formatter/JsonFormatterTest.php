<?php

use IC\Activity\Formatter\JsonFormatter;

class JsonFormatterTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function when_given_an_array_return_json()
    {
        $formatter = new JsonFormatter();

        $data = [
            'string' => 'value',
            'integer' => 10,
            'array' => [
                'k' => 'v'
            ]
        ];

        $json = $formatter->format($data);

        $this->assertSame('{"string":"value","integer":10,"array":{"k":"v"}}', $json);
    }
}
